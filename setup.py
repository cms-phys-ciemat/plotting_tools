from setuptools import setup, find_packages


setup(
    name='plotting_tools',
    version='0.1.0',
    packages=find_packages(include=['plotting_tools', 'plotting_tools.*'])
)

