from analysis_tools.utils import import_root
ROOT = import_root()

def get_cms_label(upper_left="Private work", **kwargs):
    upper_left_offset = kwargs.get("upper_left_offset", 0.0)
    x_label = 0.11 + upper_left_offset
    label = ROOT.TLatex(x_label, 0.91, "#scale[1.316]{#font[61]{CMS}} #font[52]{%s}"% upper_left)
    label.SetNDC(True)
    scaling = kwargs.get("scaling", 1.)
    label.SetTextSize(0.04 * scaling)
    return label

def get_labels(upper_left="Private work", upper_right="2018 Simulation (13 TeV)", inner_text = None, **kwargs):
    labels = [get_cms_label(upper_left, **kwargs)]
    scaling = kwargs.get("scaling", 1.)
    if upper_right:
        label = ROOT.TLatex(0.90, 0.91, "{}".format(upper_right))
        label.SetNDC(True)
        label.SetTextSize(0.04 * scaling)
        label.SetTextAlign(31)
        labels.append(label)
    if inner_text:
        y = 0.8
        for text in inner_text:
            label = ROOT.TLatex(0.15, y, "{}".format(text))
            label.SetNDC(True)
            label.SetTextSize(0.03 * scaling)
            labels.append(label)
            y -= 0.05
    return labels
